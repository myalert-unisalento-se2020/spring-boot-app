package it.unisalento.springbootapp.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Warning {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String description;
    String type;

    @OneToOne
    InterventionNote interventionNote;

    @OneToMany (mappedBy = "warning")
    List<UserWarning> userWarningList;

   /* @ManyToMany
    List<User> userList;
    */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public InterventionNote getInterventionNote() {
        return interventionNote;
    }

    public void setInterventionNote(InterventionNote interventionNote) {
        this.interventionNote = interventionNote;
    }

    public List<UserWarning> getUserWarningList() {
        return userWarningList;
    }

    public void setUserWarningList(List<UserWarning> userWarningList) {
        this.userWarningList = userWarningList;
    }


}
