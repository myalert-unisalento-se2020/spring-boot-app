package it.unisalento.springbootapp.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    String name;
    String surname;
    String type;
    String email;
    String password;

    @OneToMany(mappedBy = "user")
    List<InterventionNote> interventionNoteList;

    @OneToMany(mappedBy = "user")
    List<UserWarning> userWarningList;

    /**
     * Inverse side.
     * Cannot update the relational,
     * because was define in directly mode.
     *
     * ---------> NOT SUGGEST <-----------
     *
     * Alessandro SUGGEST to use another class
     * to describe this relationship.
     */
    /*
    @ManyToMany (mappedBy = "userList")
    List<Warning> warningList;
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<InterventionNote> getInterventionNoteList() {
        return interventionNoteList;
    }

    public void setInterventionNoteList(List<InterventionNote> interventionNoteList) {
        this.interventionNoteList = interventionNoteList;
    }

    public List<UserWarning> getUserWarningList() {
        return userWarningList;
    }

    public void setUserWarningList(List<UserWarning> userWarningList) {
        this.userWarningList = userWarningList;
    }


}
