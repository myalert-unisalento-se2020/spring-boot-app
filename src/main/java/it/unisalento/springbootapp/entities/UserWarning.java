package it.unisalento.springbootapp.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserWarning {

    /*
    * Primary key of this table.
    * */
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    int id;

    @ManyToOne
    User user;

    @ManyToOne
    Warning warning;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Warning getWarning() {
        return warning;
    }

    public void setWarning(Warning warning) {
        this.warning = warning;
    }


}
