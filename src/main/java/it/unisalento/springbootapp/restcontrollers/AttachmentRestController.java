package it.unisalento.springbootapp.restcontrollers;

import it.unisalento.springbootapp.dto.Attachment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/attachment")
public class AttachmentRestController {

    private static final String UPLOADED_FOLDER = "/Users/Mimmo/Downloads/Telegram Desktop/SpringBootApp/img/";

    @PostMapping(value = "/uploadFile")
    public ResponseEntity<?> upload(@RequestAttribute("file") MultipartFile file) throws IOException {

        System.out.println("FileName: " + file.getOriginalFilename());
        saveFile(file);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    /* TODO: NOT WORK */
    @PostMapping("/uploadFileAndInfo")
    public ResponseEntity uploadWithInfo(@ModelAttribute Attachment attachment) throws IOException {
        saveFile(attachment.getFile());
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    private void saveFile(MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        String filename = generateUUID()+"-"+file.getOriginalFilename();
        Path path = Paths.get(UPLOADED_FOLDER + filename);
        Files.write(path, bytes);
    }

    private String generateUUID (){
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
