package it.unisalento.springbootapp.restcontrollers;

import it.unisalento.springbootapp.dto.InterventionNoteDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

@RestController
@RequestMapping("/api/interventionNote")
public class InterventionNoteRestController {

    @PostMapping(
            value = "/save",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public InterventionNoteDTO post(@RequestBody InterventionNoteDTO indto){

        return indto;
    }

}
