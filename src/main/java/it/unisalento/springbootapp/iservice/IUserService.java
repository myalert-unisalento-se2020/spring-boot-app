package it.unisalento.springbootapp.iservice;

import it.unisalento.springbootapp.entities.User;
import it.unisalento.springbootapp.exceptions.UserNotFoundException;

import java.util.List;

public interface IUserService {

    User save (User user);
    List<User> getAll();
    User getById (int id) throws UserNotFoundException;
    void delete (int id) throws UserNotFoundException;
    List<User> getName(String name);


}
