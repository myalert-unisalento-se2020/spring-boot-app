package it.unisalento.springbootapp.repositories;

import it.unisalento.springbootapp.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Pattern;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findByName(String name);

    /**
     *  a is a placeholder/reference to User
     *  :name is a reference to Param value of function
     */
    @Query("select a from User a where a.name=:name")
    List<User> findByNameUsingAQuery(@Param(value="name") String name);

    @Query("select a from User a where a.name=:name and a.surname=:surname")
    List<User> findByNameAndSurnameUsingQuery(@Param(value="name") String name, @Param(value="surname") String surname);


}
