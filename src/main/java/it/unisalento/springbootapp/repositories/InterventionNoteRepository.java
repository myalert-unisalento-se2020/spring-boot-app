package it.unisalento.springbootapp.repositories;

import it.unisalento.springbootapp.entities.InterventionNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterventionNoteRepository extends JpaRepository<InterventionNote, Integer> {
}
