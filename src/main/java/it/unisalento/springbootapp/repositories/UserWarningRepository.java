package it.unisalento.springbootapp.repositories;

import it.unisalento.springbootapp.entities.UserWarning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserWarningRepository extends JpaRepository<UserWarning, Integer> {
}
