package it.unisalento.springbootapp.repositories;

import it.unisalento.springbootapp.entities.InterventionNote;
import it.unisalento.springbootapp.entities.Warning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WarningRepository extends JpaRepository<Warning, Integer> {

    List<Warning> findByType(String type);

    @Query("select w from Warning w where w.interventionNote.id=:interventionNoteId")
    List<Warning> findByIntNoteId(@Param(value="interventionNoteId") int interventionNoteId);
}
