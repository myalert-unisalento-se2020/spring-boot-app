package it.unisalento.springbootapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.unisalento.springbootapp.iactors.IActor;
import it.unisalento.springbootapp.isinger.ISinger;
import it.unisalento.springbootapp.singerimpl.Lennon;

@Controller
public class HomeController {
	
	
	@Autowired
	ISinger lennon;
	
	@Autowired
	@Qualifier
	ISinger bowie;
	
	@Autowired
	@Qualifier
	ISinger albano;
	
	@Autowired
	@Qualifier
	IActor marlonBrando;
	
	@Autowired
	IActor robertDeNiro;
	
	@Autowired
	@Qualifier
	IActor tomHanks;
	
	@RequestMapping(value="/home",method = RequestMethod.GET)
	public String getHome() {
		
		//Lennon lennon = new Lennon();
		lennon.sign();
		bowie.sign();
		albano.sign();
		
		marlonBrando.speak();
		robertDeNiro.speak();
		tomHanks.speak();
		
		//la stringa ritornata deve avere lo stesso nome di un file html
		// nel package templates
		return "home";
	}

}
