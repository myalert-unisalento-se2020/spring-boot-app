package it.unisalento.springbootapp.servicesimpl;

import it.unisalento.springbootapp.entities.User;
import it.unisalento.springbootapp.exceptions.UserNotFoundException;
import it.unisalento.springbootapp.iservice.IUserService;
import it.unisalento.springbootapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public User getById(int id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);

    }

    @Override
    @Transactional(rollbackOn = UserNotFoundException.class)
    public void delete(int id) throws UserNotFoundException{
        User user = userRepository.getOne(id);
        if (user != null) userRepository.delete(user);
        else throw new UserNotFoundException();
    }

    @Override
    public List<User> getName(String name) {
        return userRepository.findByName(name);
    }
}
