package it.unisalento.springbootapp.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NamePersonValidator implements ConstraintValidator<NamePersonConstraint,String> {
    private String name;

    @Override
    public void initialize(NamePersonConstraint constraintAnnotation) {
        this.name = constraintAnnotation.name();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !this.name.equalsIgnoreCase(value);
    }
}
