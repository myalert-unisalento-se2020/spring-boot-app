package it.unisalento.springbootapp.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = NamePersonValidator.class)
public @interface NamePersonConstraint {

    String name();

    String message() default "You shall not pass";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
