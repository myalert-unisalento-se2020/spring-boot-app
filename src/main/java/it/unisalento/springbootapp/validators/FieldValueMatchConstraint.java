package it.unisalento.springbootapp.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = FieldValueMatchValidator.class)
public @interface FieldValueMatchConstraint {

    String field();
    String fieldMatch();

    String message() default "Fields values don't match";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @Documented
    @interface List{
        FieldValueMatchConstraint[] value();
    }
}
