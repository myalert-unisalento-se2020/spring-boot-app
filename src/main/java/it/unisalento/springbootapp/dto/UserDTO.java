package it.unisalento.springbootapp.dto;

import it.unisalento.springbootapp.validators.FieldValueMatchConstraint;
import it.unisalento.springbootapp.validators.NamePersonConstraint;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@FieldValueMatchConstraint.List({
		@FieldValueMatchConstraint(
				field = "password",
				fieldMatch = "passwordVerify"
		)
})
public class UserDTO {

	int id;
	@NotBlank
	@NamePersonConstraint(name="Osvaldo")
	String name;
	@NotBlank
	String surname;
	String type;
	@Email
	String email;
	String password;
	String passwordVerify;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordVerify() {
		return passwordVerify;
	}
	public void setPasswordVerify(String passwordVerify) {
		this.passwordVerify = passwordVerify;
	}

	@Override
	public String toString() {
		return "\nName:    " + this.name + "\nSurname: " + this.surname + "\nType:    " + this.type;
	}
}
