package it.unisalento.springbootapp.dto;

import java.util.Date;
import java.util.List;

public class InterventionNoteDTO {
    int id;
    Date date;
    int duration;
    String description;
    List<Integer> attachmentsId;

    public InterventionNoteDTO() {
    }

    public InterventionNoteDTO(int id, Date date, int duration, String description, List<Integer> attachmentsId) {
        this.id = id;
        this.date = date;
        this.duration = duration;
        this.description = description;
        this.attachmentsId = attachmentsId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getAttachmentsId() {
        return attachmentsId;
    }

    public void setAttachmentsId(List<Integer> attachmentsId) {
        this.attachmentsId = attachmentsId;
    }
}
